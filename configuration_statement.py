# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.modules.currency.fields import Monetary
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.pool import Pool

class Configuration(ModelSingleton, ModelView, ModelSQL):
    "Configuration Chas Closures"
    __name__ = 'sale.cash_closures'

    company = fields.Many2One(
        'company.company', "Company", required=True, select=True)
    currency = fields.Many2One(
            'currency.currency', "Currency")

    mismatch_limit = Monetary(
        "Mismatch Limit", currency='currency', digits='currency')
    account_mismatch_charge = fields.Many2One('account.account', "Account Mismatch Charge",
                              domain=[
                                  ('company', '=', Eval('company', 0)),
                                  ('type', '!=', None),
                                  ('closed', '!=', True),
                              ],)

    account_mismatch_positive = fields.Many2One('account.account', "Account Mismatch Positivo",
                              domain=[
                                  ('company', '=', Eval('company', 0)),
                                  ('type', '!=', None),
                                  ('closed', '!=', True),
                              ],)
    @staticmethod
    def default_currency():
        Company = Pool().get('company.company')
        company = Transaction().context.get('company')
        if company:
            return Company(company).currency.id

    @staticmethod
    def default_company():
        return Transaction().context.get('company')    
