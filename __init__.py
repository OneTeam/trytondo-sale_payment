# This file is part of the sale_payment module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import device
from . import sale
from . import statement
from . import user
from . import configuration_statement

def register():
    Pool.register(
        configuration_statement.Configuration,
        statement.Journal,
        statement.Statement,
        statement.Line,
        statement.StatementLine,
        statement.LinesInvoiceToPay,
        device.SaleDevice,
        user.User,
        device.SaleDeviceStatementJournal,
        sale.Sale,
        sale.SalePaymentForm,
        sale.ChangePaymentMethodForm,
        statement.OpenStatementStart,
        statement.OpenStatementDone,
        statement.CloseStatementStart,
        statement.CloseStatementDone,
        statement.PayInvoiceSupplierStart,
        module='sale_payment', type_='model')
    Pool.register(
        sale.WizardSalePayment,
        sale.ChangePaymentMethod,
        sale.WizardSaleReconcile,
        statement.OpenStatement,
        statement.CloseStatement,
        statement.PayInvoiceSupplier,
        module='sale_payment', type_='wizard')
